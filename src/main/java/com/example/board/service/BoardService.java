package com.example.board.service;

import com.example.board.dto.BoardDto;

import java.util.List;

public interface BoardService {
    List<BoardDto> selectBoardList() throws Exception;

    void boardWrite(BoardDto boardDto);

    BoardDto boardRead(int boardSeq);

    void boardModifyForm(int boardSeq);

    void boardModify(BoardDto boardDto);

    void boardDelete(int boardSeq);
}
