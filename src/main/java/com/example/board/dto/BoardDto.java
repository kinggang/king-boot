package com.example.board.dto;

import lombok.Data;

@Data
public class BoardDto {

    private int boardSeq;
    private String title;
    private String contents;
    private String regDate;

    // Lombok으로 인해 getter(), setter() 생성할 필요 없음.
    public String toString() {
        return "BoardDto [boardSeq = " + boardSeq + ", title = " + title +
                ", contents = " + contents + ", regDate = " + regDate + " ]";
    }
}
