<%@ page import="com.example.board.dto.BoardDto" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>글 상세</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
<form name="form" method="post">

    <div class="row mb-3">
        <label for="boardSeq" class="col-sm-2 col-form-label">글번호</label>
        <div class="col-sm-10">
            <input class = "form-control" id = "boardSeq" name="boardSeq" readonly="readonly" value="${boardDto.boardSeq}">
        </div>
        <label for="regDate" class="col-sm-2 col-form-label">날짜</label>
        <div class="col-sm-10">
            <input class = "form-control" id = "regDate" name="regDate" readonly="readonly" value= "${boardDto.regDate}">
        </div>
        <label for="title" class="col-sm-2 col-form-label">제목</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="title" readonly="readonly"value="${boardDto.title}" id="title">
        </div>
    </div>
    <div class="row mb-3">
        <label for="contents" class="col-sm-2 col-form-label">내용</label>
        <div class="col-sm-10">
            <textarea class = "form-control" id = "contents" name="contents" readonly="readonly"> ${boardDto.contents}</textarea>
        </div>
    </div>
</form>

    <button class="btn btn-primary" onclick="location.href='list'">목록</button>
    <button class="btn btn-primary" id="modify" onclick="location.href='boardModifyForm?boardSeq=${boardDto.boardSeq}'">수정</button>
    <button class="btn btn-primary" id="delete" onclick="location.href='boardDelete?boardSeq=${boardDto.boardSeq}'">삭제</button>
</body>

</html>