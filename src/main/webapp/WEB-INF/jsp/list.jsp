<%@ page import="com.example.board.dto.BoardDto" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Spring Boot Application with JSP</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src = "https://code/jquery.com/jquery-1.11.3.js"></script>
</head>
<body>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">게시판</th>
            <th scope="col">제목</th>
            <th scope="col">내용</th>
            <th scope="col">작성일</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach items="${list}" var="item">
            <tr>
                <th scope="row"> ${item.boardSeq}</th>
                <td><a href="/boardRead?boardSeq=${item.boardSeq}">${item.title}</a> </td>
                <td> ${item.contents}</td>
                <td> ${item.regDate}</td>
            </tr>
        </c:forEach>

        </tbody>
    </table>

    <br>
    <button type="button" class="btn btn-primary" onclick="location.href='./boardWriteForm'">글쓰기</button>
    <button type="button" class="btn btn-secondary" onclick="location.href='/'">메인으로 가기</button>
</body>
</html>